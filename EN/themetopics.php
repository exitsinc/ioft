<?php include('include/main_header.php'); ?>
<!-- <section class="topics-ban">
    <div class="top-ban">
        <img src="images/gray-painted-background_53876-94041.jpg" class="d-block w-100" alt="...">
        <h1 style="margin: 0 auto;">Themes & Topics</h1>
    </div>
</section> -->
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8
      <h1 class="mb-4">Themes & Topics</h1>
     
    </div>
  </div>
</div> -->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Themes & Topics</h1>
      </div>
    </div>
    </div>
  </div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Themes & Topics</h1>
       
      </div>
    </div>
  </div>
</div> -->
<section class="topic-main mtb">
<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
      <h5>The main theme of the conference is Building bridges of understanding and acceptance
      between cultures and people. We welcome proposals for research papers and presentations
      from colleagues on any of the below topics and/or other topics that may be suggested.</h5>
      <ul>
        <li>The concept of tolerance</li>
        <li>Interfaith understanding and tolerance</li>
        <li>Intercultural understanding</li>
        <li>Tolerance in the corporate setting</li>
        <li>The youth and tolerance</li>
        <li>Tolerance in sports</li>
        <li>Tolerance in media</li>
        <li>Best practice</li>
        <li>Others</li>
      </ul>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
      <img src="images/img_4.jpg" class="d-block w-100">
    </div>
  </div>
</div>
</section>
<?php include('include/main_footer.php'); ?>