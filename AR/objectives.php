<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">Objectives</h1>
			</div>
		</div>
	</div>
</div>
<div class="site-section section-1 section-1-about bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-4 mb-md-4 section-title">
				<h2>Objectives</h2>
				<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </p>
			</div>
			<div class="col-lg-8">
				<div class="px-lg-3">
					<p class="dropcap">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </p><br/>
					<ul>
						<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
						<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
						<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
						<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
						<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					</ul>
				</div>
			</div>
			
		</div>
		<div class="row mt-4">
			<div class="col-lg-12 mb-md-4 section-title">
				<h1>WELCOME </h1>
				<h3>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi  ?</h3>
				<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Exercitationem enim adipisci, voluptas a fugit officia quidem minus veniam corrupti nostrum numquam corporis repudiandae sit ad laboriosam accusantium cumque asperiores eum.</p>
			</div>

			<div class="col-lg-12 mb-md-4 section-title">
				<h3>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi  ?</h3>
				<p>Lorem ipsum, dolor sit amet consectetur adipisicing, elit. Provident, veritatis, maxime. Unde eligendi harum rem, esse atque inventore perspiciatis dicta et omnis, voluptates odit molestiae fugiat? Magni enim corrupti ex!</p>
				<ul>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
					<li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php include('include/main_footer.php'); ?>