<?php include('include/main_header.php') ?>
<!-- <section class="sep-ban">
  <div class="sepk-ban">
    <img src="https://wp.the-eea.com/wp-content/uploads/2019/07/Meet-the-Speakers-banner-1-2000x750.jpg" class="d-block w-100" alt="...">
  </div>
</section> -->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Meet Our Speakers</h1>
      </div>
    </div>
    </div>
  </div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Meet Our Speakers</h1>
        
      </div>
    </div>
  </div>
</div> -->
<section class="sepk-main mtb">
  <div class="container">
    <!--  <div class="row">
      <div class="col-md-4">
        <div class="speaker-img-div">
          <img class="d-block w-100" src="images/person_2.jpg" alt="">
        </div>
      </div>
      <div class="col-md-6">
        <h5>Professor King Cheek</h5>
        <p class="">Dr. King Virgil Cheek, J.D. is a lifelong educator who has served as President of Shaw
          University from 1969 to 1971, and also served as the 8th President of Morgan State
          University from 1971 to 1974.  He is the author of numerous books including The Quadrasoul,
          novels that explore the four dimensions of the human spirit. He is member of the Board of
        Trustees of the International Foundation for Tolerance.</p>
      </div>
    </div> -->
    
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <h1>Our Speakers</h1>
        <!-- <hr> -->
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/King (1).jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Professor King Cheek</h5>
                <p class="card-text">Dr. King Virgil Cheek, J.D. is a lifelong educator who has served as President of Shaw University from 1969 to 1971, and also served as the 8th President of Morgan State University from 1971 to 1974.  He is the author of numerous books including The Quadrasoul, novels that explore the four dimensions of the human spirit. He is member of the Board of Trustees of the International Foundation for Tolerance. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">H.E  Altay Abibullayev</h5>
                <p class="card-text"> His Excellency Altay Abibullayev is the Chairman of the Board of Nursultan Nazarbayev Center for Development of Interfaith and Inter-Civilization Dialogue in Kazakhstan. He was the former spokesman of Kazakh MFA-Chairman Committee for International Information and former Spokesman of RK - Deputy Director of CCS and former deputy Head of Agency for communication and informational of Kazakhstan. </p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/HE 1.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/caroline makaka.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. Caroline Makaka, United Kingdom</h5>
                <p class="card-text">Multi Award Winner Dr Caroline Makaka is the Founder/CEO of Ladies of All Nations International also known as LOANI. Caroline has been recognized by numerous by numerous & Prominent Bodies. She is a Mother, Patron an Orphange School in Africa , Board Advisor for Central Entrepreneur Capital Union , President of Aesthetics International UK , Save the Girl Child Ambassador</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Joseph K. Grieboski</h5>
                <p class="card-text">Bishop Joseph K. Grieboski is the president of the International Center on Religion and Justice in Washington DC, United States of America. He is a member of the board of trustees of the International Foundation for Tolerance, the Board of Directors of Interfaith Alliance, and is completing two doctoral degrees in theology and psychology.</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/j.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Anil.jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Prof. Anil Srivastav, India</h5>
                <p class="card-text">He is the chairman of the North Eastern India Chamber of Commerce and Industry. He is a visiting professor at MPISSR, Ujjain (M.P.) associated with Indian Council of Social Science Research. He has received numerous awards both in India and overseas for his role in building business and cultural bridges among different nations. He is also a member of the advisory council of the international foundation for tolerance and member of the board of many other regional and international organizations.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. Ghadha Al Murshidi, United Arab Emirates</h5>
                <p class="card-text">Dr. Ghadah is assistant professor in the college of education, the United Arab Emirates university. She has her PhD degree in Curriculum and Instruction and Comparative and International Education, Pennsylvania State University, MA in educational leadership and another master degree in applied linguistics from Pennsylvania State University. In addition to her academic role, she is also a professional trainer for a wide range of training programs.</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Dr. Ghadah Al Murshidi's photo.png" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/robertcrane.png" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. Robert Crane USA/ France</h5>
                <p class="card-text">Robert Crane has built a solid reputation in international development, cross-cultural
                  business applications, and institutional entrepreneurship worldwide. During his career, he
                  has been involved with such management institutions as the J.L. Kellogg school of
                  Management at Northwestern University, EM-Lyon in France and the International
                  Management Center (now the business school of Central European University) in
                  Budapest. He has developed and run customized programs for senior executives of such
                  firms as Société Générale, Nokia, Bouygues, McKinsey and Co. and many others. He is
                  publishing a series of cross cultural management books with Palgrave-MacMillan in
                  London.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. George Simons , USA / France</h5>
                <p class="card-text">Dr. Simons holds a Doctorate from Claremont Graduate School in Psychology. His online collaboration in writing projects resulted in books on EuroDiversity, Global Competence, Working Together, Putting Diversity to Work, Men & Women, Partners at Work and Transcultural Leadership. He is the creator and/or editor of over fifty games in the diversophy® series of training room and online cultural training games and has authored eight instruments in the Cultural Detective® series. He has coauthored The Questions of Diversity and the Cultural Diversity Fieldbook and the Cultural Diversity Sourcebook.</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/george_simons.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Indunil 2.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Indunil Fernando, Sri Lanka</h5>
                <p class="card-text">Indunil is currently the Chief Executive Officer  of a major Vetenerary hospital in Sri Lanka. He has more than 21 years of experience across multiple industries & business verticals with a track record of producing, presenting and managing the implementation of innovative business solutions. He is also a regional director of Global Goodwill Ambassadors.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. Mai Nguyen, The Netherland</h5>
                <p class="card-text">Dr. Mai Nguyen (or Nguyen-Phuong-Mai) is Associate Professor at AMSIB where she has been working since 2008. She involves in diverse research projects. In the last few years, she has taken interest in cultural neurosciences and published her latest book Cross-Cultural Management with Insights from Brain Science  . Dr. Mai Nguyen holds a Master in Educational Science and Curriculum Design from Twente University, and a PhD in Intercultural Communication from Utrecht University, The Netherlands. She contributes as member of several research advisory boards, including ELLTA (Leadership and Learning in the Asian century).</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Phuong-Mai-Nguyen_avatar_1410051826-190x190.jpeg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Ewelina.jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Ewelina U. Ochab, United Kingdom</h5>
                <p class="card-text">Ewelina is a legal researcher and human rights advocate, and author of the book “Never Again: Legal Responses to a Broken Promise in the Middle East.” She works on the topic of persecution of minorities around the world, with primary projects including Daesh genocide in Syria and Iraq, Boko Haram atrocities in West Africa, and the situation of religious minorities in South Asia. She has written over 30 UN reports and has made oral and written submissions at the Human Rights Council sessions and the UN Forum on Minority Issues. She is working on her PhD in international law, human rights and medical ethics.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. Earl Johnson, USA</h5>
                <p class="card-text">President and CEO of International Consultants and Investigations, Inc. (ICI), He leads
                  investigation of international financial fraud, intelligence gathering, counter terrorism, corporate
                  security and investigation of maritime accidents. He has been involved in recent year with cyber
                security providing consulting services to leading companies in the USA and other countries.</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/earl johnson.png" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/rajiv.jpeg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Professor Rajiv Gupta, India </h5>
                <p class="card-text">Dr. Gupta is a professor at the Institute of Management Studies, Devi Ahilya Vishwavidyalaya,
                  and director, Directorate of Distance Education, Devi Ahilya Vishwavidyalaya, India. He held
                  many positions in the past including the dean of the faculty of management studies at Devi
                  Ahilya Vishwavidyalaya. He has published many articles and papers and spoke at different
                regional and international conferences. </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">KELVIN MORGAN, Ghana  </h5>
                <p class="card-text">Dr. Kevin Morgan is the co-founder of the African Festival Foundation and he is also the
                  executive director, international relations for Logest Gorup in Ghana. He is an activist and
                  speaker for and peace and youth development. He is a member of many national, regional and
                  international organizations. He was awarded an Honorary Doctorate from the Council of
                Alternative system of medicine.</p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Dr Kevin Morgan.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Jeleena.jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Jelena M. Dimitrijević, Serbia  </h5>
                <p class="card-text">Jelena M. Dimitrijević is a certified systems psychodynamic organizational consultant and executive coach, a member of the International Society for the Psychoanalytic Studies of Organizations. Her 20-year long journey through the corporate world, international organizations and NGO sector, included positions of a member of full-time staff, external or internal expert consultant. For several years now her attention has been turned towards process consultancy. Using systems psychodynamic principles, she supports individuals, teams and organizations. By stimulating leadership and authority distribution and resolving conflicts arising at work, she helps improve and strengthen organizational culture in organizations
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Mr. Sadeque Hussain, India  </h5>
                <p class="card-text">Sadeque Hussain is currently he is leading a World Bank Program on Disaster Management through community participation in India. He has completed his masters in social work and worked briefly as a research officer in the Indian Institute of Public Administration. Basically a development consultant with over 20 years of experience in leading humanitarian organizations in the domain of Disaster risk resilience, Social Development and good governance and positioned in most challenging areas. He also initiated a peace building network at global level.
                </p>
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Sadiqu Hussain.jpg" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
             <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/Victoria 2.jpg" alt="Card image cap">
              </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Victoria Aniesineno  Umoren, Nigeria   </h5>
                <p class="card-text">Victoria Umoren is a young Nigerian member of Blue Sky Development Foundation and actively involved in Give Back Project. She works with volunteers to provide food and support to the so many Internally Displaced Persons (IDP). She will address the critical link between tolerance and development in Africa.
                </p>
              </div>
            </div>
           
          </div>
        </div>
      </div>
    </div>
        <div class="row speakers">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="card speaker-card">
          <div class="row">
             
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
              <div class="card-body">
                <h5 class="card-title">Dr. Melfi Alrasheedi, Saudi Arabia</h5>
                <p class="card-text">Dr. Alrasheedi is the head of the Quantitative Methods department at the School of Business, King Faisal University in Saudi Arabia.  He holds a PhD degree in Operations Research from Brunel University, United Kingdom. He HAS held many OTHER positions including the deanship of a community college. He has translated three textbooks and published many papers in international professional journals.
                </p>
              </div>
            </div>
           <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
              <div class="speaker-img-div">
                <img class="card-img d-block w-100 speaker-img" src="images/7.png" alt="Card image cap">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Layla Habib Albloushi, United Arab Emirates    </h5>
              <p class="card-text">Dr. Layla is a management and cross-cultural consultant and family counsellor. With more than 25 years of experience in banking and international management, she has conducted a wide range of training programs for government and private organizations in the UAE and abroad. Among her international clients are Airbus, Nestle Oil, Borealis and others. She is a member of the board of trustees of the International Foundation for Tolerance .</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/layla.jpg" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Mohamed Mousa BabaAmmi, Algeria </h5>
              <p class="card-text">He is one of the leading scholars in the Arab world. He is a university professor, an author and founder of many educational and social initiatives and projects. He has more than 35 published books. Dr. Mohamed has a PhD in Islamic studies. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Idris Maghari, Morocco</h5>
              <p class="card-text">He is a lecturer and trainer in the ministry of higher education in Morocco for the subject of sports and physical education. He is an assessor of many regional sports events and awards. He has a master and PhD in sports philosophy.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Melfi Alrasheedi, Saudi Arabia </h5>
              <p class="card-text">Dr. Alrasheedi is the head of the department of quantitative methods, school of business at King Faisal University in Saudi Arabia. He has a PhD in operations research from Brunel University and a master in applied statistics from Oregon State University. He has translated three books from English to Arabic and wrote a number of published articles. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Kawther Al Zeglami , Tunisia   </h5>
              <p class="card-text">Ms. Kawather is a faculty member of a major educational establishment in Tunisia. She is finalizing her doctoral dissertation to be completed this year 2020. Her areas of specialization is cross cultural communication, social change and women empowerment. She is an active member in the community offering many social initiatives and programs.  </p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Nabeel Al Ketheeri , UAE  </h5>
              <p class="card-text">Nabeel is an author and media specialist. He has published novels as well as other books in topics relevant to the interest of the youth at home. He has a wide social media network. He uses social media groups for educational and cultural discussions. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dianne Hofner Saphiere, México/USA</h5>
              <p class="card-text">An intercultural organizational consultant since 1979 and founder of Cultural Detective, Ms. Saphiere has worked with some of the world’s largest multinationals and people from over 130 nations. Her work has focused on virtual teamwork, innovation, bridging and leveraging differences as assets, and bringing our full and authentic selves to our work and communities. She is on the faculty of various institutes worldwide, and a frequent researcher and author of books and learning simulations.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/DianneDetectClose.jpg" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Sameeh AlMejali, Jordan </h5>
              <p class="card-text">Dr. AlMejali is a lecturer at the Arab League center for security studies in Jordan. His doctoral degree is in social sciences specializing in criminology. He has a number of research papers in violence against women, impact of social media on criminal behaviours etc. </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row speakers">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Amin Abu Hejla, Jordan </h5>
              <p class="card-text">Dr. Amin has a PhD in strategic studies from Aberdeen university. He is the president of Peace Knights in Jordan. He is a member of the board of many other international human rights and peace organizations.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers" >
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/img-no.jpg" alt="Card image cap">
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Jamal AlMukhtar, Iraq</h5>
              <p class="card-text">Dr. AlMukhtar has a PhD in business administration from Al Mosel Universty in Iraq. He is the head of supply chain department in the Technology Institute in Mosel. He a number of books and publications on entrepreneurship and business incubation.  </p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row speakers" style="padding-bottom: 0px !important;">
      <div class="card speaker-card">
        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
            <div class="card-body">
              <h5 class="card-title">Dr. Earl Johnson, USA </h5>
              <p class="card-text">President and CEO of International Consultants and Investigations, Inc. (ICI), He leads
                investigation of international financial fraud, intelligence gathering, counter terrorism, corporate
                security and investigation of maritime accidents. He has been involved in recent year with cyber
              security providing consulting services to leading companies in the USA and other countries.</p>
            </div>
          </div>
          <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4 d-flex justify-content-center align-items-center">
            <div class="speaker-img-div">
              <img class="card-img d-block w-100 speaker-img" src="images/earl johnson.png" alt="Card image cap">
            </div>
          </div>
        </div>
      </div>
    </div> -->
  </div>
</section>
<?php include('include/main_footer.php') ?>