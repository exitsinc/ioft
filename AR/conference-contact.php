<?php include('include/main_header.php'); ?>


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
       <h1 class="mb-4">تواصل معنا</h1>
      </div>
    </div>
  </div>
</div>


<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">تواصل معنا</h1>
        <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium facere officiis in quo, corporis quasi.</p>
        <p><a href="#" class="btn btn-primary px-4 py-3">Get Started</a></p> 
      </div>
    </div>
  </div>
</div> -->



<div class="site-section right-text-class">
  <div class="container">
    <div class="row">
      <!-- <div class="col-lg-12 justify-content-center">
        <p class="lead">للتواصل</p>
        <p class="lead">يمكن التواصل من خلال تعبئة النموذج او ارسال ايميل مباشرة</p>
        
      </div> -->
      <!-- <div class="col-md-12 col-lg-7 mb-5">
        <form action="#" class="contact-form">
          <div class="row form-group">
            <div class="col-md-12 mb-3 mb-md-0">
              <label class="font-weight-bold" for="fullname">الاسم الكامل</label>
              <input type="text" id="fullname" class="form-control" placeholder="الاسم الكامل">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="email">البريد الإلكتروني</label>
              <input type="email" id="email" class="form-control" placeholder="عنوان البريد الالكترونى">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="email">موضوع</label>
              <input type="text" id="subject" class="form-control" placeholder="أدخل الموضوع">
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <label class="font-weight-bold" for="message">رسالة</label>
              <textarea name="message" id="message" cols="30" rows="5" class="form-control"
              placeholder="قل مرحبا لنا"></textarea>
            </div>
          </div>
          <div class="row form-group">
            <div class="col-md-12">
              <input type="submit" value="أرسل رسالة" class="btn btn-primary py-3 px-4">
            </div>
          </div>
        </form>
      </div> -->
      <div class="col-lg-12 ml-auto">
        <div class="p-4 mb-3 bg-white cont">
          <h3 class="h5 text-black mb-3">للتسجيل ومزيد من المعلومات  </h3>
          <p class="mb-0 font-weight-bold text-black">  امانة المؤتمر</p>
          <p class="mb-4 text-black">الهيئة الدولية للتسامح  </p>
         
          
          <p class="mb-0"><a href="#">info@iftolerance.org</a></p>
        </div>
      </div>
    </div>
  </div>
</div>

<section class="conf-con-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <div id="paypal-button-container"></div>
      </div>
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
          <div class="google-reg">
            <img src="images/reg-400x300.png" alt="">
          </div>
        </a>
      </div>


    </div>
  </div>
</section>
<!-- <div class="site-section section-6 bg-light">
  <div class="container">
    <div class="row">
      <div class="col-lg-5 mr-auto">
        <h2 class="mb-5">Happy <span class="text-primary">Clients Says</span></h2>
        <div class="owl-carousel nonloop-block-4 testimonial-slider">
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
          <div class="testimony-1">
            <div class="d-flex align-items-center mb-4">
              <img src="images/img_sq.jpg" alt="Image" class="vcard mr-4">
              <div>
                <h3>John Doe</h3>
                <p>Business Man</p>
              </div>
            </div>
            <blockquote>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptas cumque asperiores mollitia. Aperiam vel excepturi perspiciatis debitis error magnam odit alias mollitia est totam tempora ullam magni, officia, hic at!</p>
            </blockquote>
          </div>
        </div>
      </div>
      <div class="col-lg-6">
        <h2 class="mb-5">Frequently <span class="text-primary">Ask Questions</span></h2>
        <div class="border p-3 rounded mb-2 bg-white">
          <a data-toggle="collapse" href="#collapse-1" role="button" aria-expanded="true" aria-controls="collapse-1" class="accordion-item h5 d-block mb-0">Law assistance to my business</a>
          <div class="collapse show" id="collapse-1">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2 bg-white">
          <a data-toggle="collapse" href="#collapse-4" role="button" aria-expanded="false" aria-controls="collapse-4" class="accordion-item h5 d-block mb-0">The newest part of my legislation</a>
          <div class="collapse" id="collapse-4">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2 bg-white">
          <a data-toggle="collapse" href="#collapse-2" role="button" aria-expanded="false" aria-controls="collapse-2" class="accordion-item h5 d-block mb-0">Are you an internationla law?</a>
          <div class="collapse" id="collapse-2">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
        <div class="border p-3 rounded mb-2 bg-white">
          <a data-toggle="collapse" href="#collapse-3" role="button" aria-expanded="false" aria-controls="collapse-3" class="accordion-item h5 d-block mb-0">How the system works?</a>
          <div class="collapse" id="collapse-3">
            <div class="pt-2">
              <p class="mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corrupti esse voluptates deleniti, ratione, suscipit quam cumque beatae, enim mollitia voluptatum velit excepturi possimus odio dolore molestiae officiis aspernatur provident praesentium.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> -->
<!-- <div class="bg-primary" data-aos="fade">
  <div class="container">
    <div class="row">
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-facebook text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-twitter text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-instagram text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-linkedin text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-pinterest text-white"></span></a>
      <a href="#" class="col-2 text-center py-4 social-icon d-block"><span class="icon-youtube text-white"></span></a>
    </div>
  </div>
</div> -->
<?php include('include/main_footer.php'); ?>