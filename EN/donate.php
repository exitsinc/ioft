<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Donate</h1>
      </div>
    </div>
    </div>
  </div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">Donate</h1>
				
			</div>
		</div>
	</div>
</div> -->
<section class="donate mtb right-text-class">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="jumbotron">
					<h1 class="display-4">Donate</h1>
					<p class="lead">The International Foundation for Tolerance is a not for profit
						professional organization where it depends on its financial
						resources from individual and corporate contributions. We invite
						you all to make a contribution. There is nothing called too little
					because every penny counts.</p>
					<p>Please fill in your name and email and follow the page for
					payment</p>
					<ul>
						<li>US$ 10</li>
						<li>US$ 25</li>
						<li>US$ 50</li>
						<li>US$ 100</li>
						<li>US$ 1000</li>
					</ul>
					<hr class="my-4">
					<p>You can contact us direct for questions and details.</p>
					<a href="info@iftolerance.org">info@iftolerance.org</a>
				</div>
			</div>
		</div>
		
	</div>
</section>
<section class="donation-form mtb right-text-class">
	<div class="container dnt-form">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="heading">
					<h1>Donation</h1>
					<hr>
				</div>
				<form>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputName">First name</label>
							<input type="text" class="form-control" placeholder="First name">
						</div>
						<div class="form-group col-md-6">
							<label for="inputLastname">Last name</label>
							<input type="text" class="form-control" placeholder="Last name">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputOrganization">Organization</label>
							<input type="text" class="form-control" placeholder="Organization">
						</div>
						<div class="form-group col-md-6">
							<label for="inputWorkphone">Work Phone</label>
							<input type="text" class="form-control" placeholder="Work Phone">
						</div>
					</div>
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputEmail4">Email</label>
							<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
						</div>
						<div class="form-group col-md-6">
							<label for="inputPassword4">Amount ($USD)</label>
							<input type="Amount" class="form-control" id="inputAmount" placeholder="Amount ($USD)">
						</div>
					</div>
					<fieldset class="form-group">
						<div class="row">
							<legend class="col-form-label col-sm-2 pt-0">Payment frequency</legend>
							<div class="col-sm-10">
								<div class="form-check">
									<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
									<label class="form-check-label" for="gridRadios1">
										Monthly
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
									<label class="form-check-label" for="gridRadios2">
										Annually
									</label>
								</div>
								<!-- <div class="form-check">
											<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3">
											<label class="form-check-label" for="gridRadios3">
														Quarterly
											</label>
								</div>
								<div class="form-check">
											<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios4" value="option4">
											<label class="form-check-label" for="gridRadios4">
														Semi-annually
											</label>
								</div>
								<div class="form-check">
											<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios5" value="option5">
											<label class="form-check-label" for="gridRadios5">
														Annually
											</label>
								</div> -->
							</div>
						</div>
					</fieldset>
					<div class="form-group">
						<label for="inputAddress">Address</label>
						<input type="text" class="form-control" id="inputAddress" placeholder=" ">
					</div>
					<div class="form-row">
						<div class="form-group col-md-3">
							<label for="inputCity">City</label>
							<input type="text" class="form-control" id="inputCity">
						</div>
						<div class="form-group col-md-3">
							<label for="inputCountry">Country</label>
							<input type="text" class="form-control" id="inputCountry">
						</div>
						<div class="form-group col-md-3">
							<label for="inputState">State</label>
							<input type="text" class="form-control" id="inputState">
						</div>
						<div class="form-group col-md-3">
							<label for="inputZip">Zip</label>
							<input type="text" class="form-control" id="inputZip">
						</div>
					</div>
					<div class="form-group">
						<label for="inputCity">Comment</label>
						<textarea type="text" class="form-control" id="inputCity"></textarea>
					</div>
					<!-- <div class="form-row form-group">
								<div class="form-group col-md-3">
											<label for="inputEmail4">Security check code :</label>
											
								</div>
								<div class="form-group col-md-3">
							<label for="inputEmail4"><?php echo uniqid(); ?></label>
							
						</div>
						<div class="form-group col-md-6">
							<label for="inputPassword4">Code</label>
							<input type="Amount" class="form-control" id="inputAmount" placeholder="Enter Security check code">
						</div>
					</div> -->
					<div class="form-group row">
						<div class="col-sm-6 pb-2">
							<button type="Cancel" class="btn btn-primary">Cancel</button>
						</div>
						<div class="col-sm-6">
							<!-- <div id="paypal-button-container"></div> -->
							<!-- <button type="pay" class="btn btn-primary">Pay</button> -->
							<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

<table>

<tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">

               <!--  <option value="المؤتمر الاسيوي للتسامح">المؤتمر الاسيوي للتسامح $25.00 USD</option>

                <option value="مؤتمر التسامح في الرياضة">مؤتمر التسامح في الرياضة $30.00 USD</option>

                <option value="مؤتمر التسامح في التعليم">مؤتمر التسامح في التعليم $30.00 USD</option> -->

                <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>

                <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>

                <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>

</select> </td></tr>

</table>

<input type="hidden" name="currency_code" value="USD">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

</form>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>
<?php include('include/main_footer.php'); ?>