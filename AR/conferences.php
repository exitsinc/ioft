<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
         <h1 class="mb-4">  المؤتمر الاقليمي الاسيوي للتسامح عبر الثقافات    <br>16 – 17 نوفمبر 2020 </h1>
      </div>
    </div>
  </div>
</div>


<section class="conferences-main mt-5  right-text-class">
  <div class="container">
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div>
          <!-- <h1 class="display-4">Hello, world!</h1> -->
          <p class="lead">نيابة عن زملائي اعضاء مجلس الامناء والمجلس الاستشاري للهيئة الدولية للتسامح  يسرني ان ادعوكم  للمشاركة في المؤتمر الاقليمي الاسيوي للتسامح عبر الثقافات الذي سيعقد على مدى يومي 16 و 17 نوفمبر 2020 . ونظرا للظروف غير الطبيعية التي يمر بها العالم نتيجة جائحة كورونا كوفيد 19 فان المؤتمر سيعقد افتراضيا عبر منصة المؤتمرات الافتراضية زوم .</p>
          <p class="lead">يستضيف المؤتمر نخبة ممتازة من الباحثين والمحاضرين من مختلف دول العالم ولذلك فان المحاضرات ستكون باللغتين العربية والانجليزية حيث ستكون هناك حوالي 2 الى 3 جلسات في نفس الوقت بعضها باللغة العربية والبعض باللغة الانجليزية لتتاح لكل مشارك حرية الاختيار بين الجلسات ليس على اساس اللغة فقط ولكن حسب الموضوعات والاهتمامات ، كما سيتم توزيع جميع اوراق العمل والبحوث في كتيب الكتروني .</p>
          <p class="lead">التسجيل في المؤتمر مجاني بدون اية رسوم والتسجيل مفتوح للجميع في اي مكان في العالم ، وبالنسبة للراغبين في الحصول على شهادة حضور المؤتمر فهناك رسوم بسيطة لاصدار الشهادات .
            نتطلع للقاء بكم افتراضيا في يوم التسامح العالمي في افتتاح المؤتمر يوم 16 نوفمبر متمنين لكم الصحة والعناية والوقاية .
          </p>
          
          <!-- <p class="lead">Dr. King V Cheek <br> Conference Chair</p> -->
          <p class="lead">
            الدكتور كينج شيك
          <br> رئيس مجلس امناء الهيئة الدولية للتسامح </p>
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>