<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Advisory Board</h1>
      </div>
    </div>
  </div>
</div>
<div class="site-section section-6 advisory-board-section">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2>ADVISORY BOARD</h2>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/DR.-GEORGE-F.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3>Dr. George Simons</h3>
          <p>CEO, Diversophy</p>
          <p>France</p>
         <!--  <a href="javascript:void();">CEO, Diversophy</a> -->
          <!-- <blockquote>
            <p>Sandra M. Fowler entered the intercultural field in the 1970s researching the Peace Corps PRIST induction program for the National Bureau of Standards Applied Psychology division, followed by 8 years of U.S. Navy personnel research during which the simulation game BaFa BaFa was developed. She directed the Navy’s Overseas Duty Support Program from 1979 to 1989, was president of SIETAR International in 1988, followed by 20 years as an independent consultant and trainer. Her publications include the 2-volume Intercultural Sourcebook of Cross-Cultural Training plus many articles and chapters in a variety of books on intercultural issues.</p>
          </blockquote> -->
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Dr.-Robert-Crane.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Dr. Robert Crane</h3>
          <p>International scholar</p>
          <p>France</p>
          
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Dianne-Hofner-Saphiere.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Dianne Hofner Saphiere</h3>
          <p>President<br/>
          Cultural Detective</p>
          <p>USA</p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Dr.-Earl-Johnson.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Dr. Earl Johnson</h3>
          <a href="javascript:void();">President<br/>ICI USA</a>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Daisy-Khan.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Daisy Khan</h3>
          <p>Founder and Executive Director</p>
          <p>Women’s Islamic initiative in Spirituality and Equality</p>
          <p>USA</p>
          
        </div>
      </div>
       <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center mb-4">
            <img src="images/Professor-Dr.-Anil-Srivastav.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3>Professor Dr. Anil Srivastav</h3>
          <p>Chairman</p>
          <p>North Eastern India-ASEAN Chamber of Commerce & Industry</p>
          <p>India </p>
         
        </div>
      </div>

      
     
    </div>
  </div>
</div>
<?php include('include/main_footer.php'); ?>