<?php include('include/main_header.php'); ?>
<!-- <section class="topics-ban">
	<div class="top-ban">
		<img src="images/gray-painted-background_53876-94041.jpg" class="d-block w-100" alt="...">
		<h1 style="margin: 0 auto;">Themes & Topics</h1>
	</div>
</section> -->

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4"> المحاور والموضوعات
  </h1>
      </div>
    </div>
  </div>
</div>


<section class="sport-conference-logo mtb">
  <div class="container">
    <div class="row sport-logo">
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo europe.jpg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2 mb-xs-5">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-13 at 21.23.00 (1).jpeg" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/rowad 3.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/logo YUOI.png" alt="">
        </div>
      </div>
      <div class="col-xs-12 col-sm-2 col-md-2 col-lg-2 col-xl-2">
        <div class="sport-logo-BImg">
          <img src="images/WhatsApp Image 2020-09-18 at 17.10.32 (2).jpeg" alt="">
        </div>
      </div>
    </div>
  </div>
</section>



<section class="topic-main mtb text-right">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
				<h5> الاطار العام للمؤتمر هو تسليط الضوء على اهمية بناء جسور التسامح الايجابي من خلال الرياضة والاعلام الرياضي وستتناول البحوث والمحاضرات المحاور التالية 

 </h5>
        
           <ul dir="rtl">
           	<li>الولاء والانتماء في الرياضة </li>
           	<li> بناء جسور التسامح بين المشجعين والجمهور  </li>
           	<li>سيكولوجية الرياضة 
 </li>
           	<li>التسامح بين المحترفين في الرياضة 
 </li>
           	<li> التسامح في اوقات الازمات  </li>
           	<li>التسامح في الاعلام الرياضي</li>
           	<li> التسامح في وسائل التواصل الاجتماعي </li>
           	<li>افضل الممارسات  </li>
           	<li> موضوعات اخرى  </li>
           <!-- <li>  افضل الممارسات عالميا </li>
           <li>  موضوعات اخرى</li> -->
           </ul>
			</div>				
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
				<img src="images/img_4.jpg" class="d-block w-100">
			</div>				
		</div>
	</div>
</section>
<?php include('include/main_footer.php'); ?>