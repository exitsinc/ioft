<?php include('include/main_header.php'); ?>


<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">Other Conferences</h1>
      </div>
    </div>
  </div>
</div>



<section class="regi-main mtb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="regdata">
          <div class="jumbotron jumbotron-fluid">
            <div class="container">
              <h1 class="display-4">Other Conferences</h1>
              <hr>
              <ul>
                <!-- <li>Tolerance in Sport Conference January 2021</li>
                <li>The African Regional Tolerance Across Cultures Conference, March 2021</li>
                <li>The European Regional Tolerance Across Cultures Conference, April 2021</li>
                <li>The American Regional Tolerance Across Cultures Conference, May 2021</li> -->
                <li>The African Regional Tolerance Across Cultures Conference, March 2021</li>
                <li>The European Regional Tolerance Across Cultures Conference, April 2021</li>
                <li>The American Regional Tolerance Across Cultures Conference, May 2021 </li>
              </ul>
              <!-- <p class="lead">
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
              proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</a></p> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>