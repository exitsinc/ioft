$(document).ready(function() {
  // Add smooth scrolling to all links
  $(".smooth-scroll-class").on('click', function(event) {

    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate({
        scrollTop: $(hash).offset().top
      }, 800, function() {

        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if
  });

  // navbar toggle click open and click close
  //   $(document).click(function(event) {
  //     var clickover = $(event.target);
  //     var _opened = $(".dropdown-menu").hasClass("dropdown-menu in");
  //     if (_opened === true && !clickover.hasClass("dropdown-item")) {
  //       $("dropdown-item").click();
  //     }
  //   });
  $(".dropdown-toggle").on("mouseenter", function() {
    // make sure it is not shown:
    if (!$(this).parent().hasClass("show")) {
      $(this).click();
    }
  });
  $(".btn-group, .dropdown").on("mouseleave", function() {
    // make sure it is shown:
    if ($(this).hasClass("show")) {
      $(this).children('.dropdown-toggle').first().click();
    }
  });



  //////////////////////// Prevent closing from click inside dropdown
  $(document).on('click', '.test-menu .dropdown-menu', function(e) {
    e.stopPropagation();
  });

  // make it as accordion for smaller screens
  if ($(window).width() < 992) {
    $('.test-menu .dropdown-menu a').click(function(e) {
      e.preventDefault();
      if ($(this).next('.submenu').length) {
        $(this).next('.submenu').toggle();
      }
      $('.dropdown').on('hide.bs.dropdown', function() {
        $(this).find('.submenu').hide();
      })
    });
  }

});