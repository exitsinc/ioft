<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4"> المجلس الاستشاري      </h1>
      </div>
    </div>
  </div>
</div>
<div class="site-section section-6 advisory-board-section right-text-class">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-center">
        <h2> المجلس الاستشاري      </h2>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/george_simons.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              <!-- <p>Business Man</p> -->
            </div>
          </div>
          <h3> الدكتور جورج سيمونز   </h3>
          <p>مؤسس ورئيس شركة دايفرسوفي  </p>
          <p>فرنسا  </p>
         <!--  <a href="javascript:void();">CEO, Diversophy</a> -->
          <!-- <blockquote>
            <p>Sandra M. Fowler entered the intercultural field in the 1970s researching the Peace Corps PRIST induction program for the National Bureau of Standards Applied Psychology division, followed by 8 years of U.S. Navy personnel research during which the simulation game BaFa BaFa was developed. She directed the Navy’s Overseas Duty Support Program from 1979 to 1989, was president of SIETAR International in 1988, followed by 20 years as an independent consultant and trainer. Her publications include the 2-volume Intercultural Sourcebook of Cross-Cultural Training plus many articles and chapters in a variety of books on intercultural issues.</p>
          </blockquote> -->
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Robert Crane 2.png" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> الدكتور روبرت كراين    </h3>
          <p>استاذ جامعي وباحث اكاديمي  </p>
          <p>فرنسا  </p>
          
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/DianneDetectClose.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> ديانا سفير هوفنر     </h3>
          <!-- <p>رئيس<br/>
           المخبر الثقافي </p>
          <p>الولايات المتحدة الامريكية </p> -->
          <p>مؤسس ورئيسة شركة  </p>
          <p>الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/earl johnson.png" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> الدكتور ايرل جونسون     </h3>
          <p>مؤسس ورئيس شركة الاستشارات الدولية  </p>
          <p>الولايات المتحدة الامريكية  </p>
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Daisy-Khan.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> ديزي خان     </h3>
          <p>مؤسس والمدير التنفيذي لمبادرة المراة المسلمة   </p>
          <p>الولايات المتحدة الامريكية  </p>
          
        </div>
      </div>
       <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Anil.jpg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> الدكتور انيل سيرفاستاف     </h3>
          <p>رئيس غرفة تجارة وصناعة شمال شرق الهند    </p>
          <p>نيودلهي – الهند  </p>
        
         
        </div>
      </div>
      <div class="col-lg-4 mr-auto  mt-5">
        <div class="testimony-1">
          <div class="d-flex align-items-center justify-content-end mb-4">
            <img src="images/Indunil 2.jpeg" alt="Image" class="vcard mr-4">
            <div>
              
              
            </div>
          </div>
          <h3> اندونيل فرناندو    </h3>
          <p>الرئيس التنفيذي لمركز العناية الصحية    </p>
          <p>سيرلانكا  </p>
        </div>
      </div>
   
    </div>
  </div>
</div>
<?php include('include/main_footer.php'); ?>