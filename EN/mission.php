<?php include('include/main_header.php'); ?>

    <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
      <div class="container">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
            <h1 class="mb-4">Mission</h1>
          </div>
        </div>
      </div>
    </div>

<div class="site-section section-1 section-1-about bg-light">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 mb-md-4 section-title">
        <h2>MISSION</h2>
        <p>Our mission is to build bridges of understanding and acceptance among different people, Groups and cultures through research and scientific approaches for the betterment of humankind.</p>
      </div>
      <div class="col-lg-8">
        <div class="px-lg-3">
          <p class="dropcap">The Goals of IFT are…</p><br/>
          <ul>
            <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
            <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
            <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
            <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
            <li>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </li>
          </ul>
        </div>
      </div>
      
    </div>
  </div>
</div>
<?php include('include/main_footer.php'); ?>