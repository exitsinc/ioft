<?php include('include/main_header.php'); ?>
<!-- <section class="reg-ban">
  <div class="regi-ban">
    <img src="https://ctusms.com/wp-content/uploads/2019/01/registration-banner-1200x400.jpg" class="d-block w-100" alt="...">
  </div>
</section>
-->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Registration</h1>
      </div>
    </div>
  </div>
</div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Registration</h1>
        
      </div>
    </div>
  </div>
</div> -->
<section class="regi-main mtb">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="regdata">
          <div class="jumbotron jumbotron-fluid">
            <div class="container">
              <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                  <h1 class="display-4">Register for the Asian Regional Tolerance Across Cultures Conference</h1>
                  <hr>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
                    <div class="google-reg aregistration">
                      <img src="images/reg-400x300.png" alt="">
                    </div>
                  </a>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <p class="lead">To register for the Asian Regional Tolerance Across Cultures Conference, please click here to fill in the form. If you have already registered, please do not register again. </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  <p class="lead">If you would like to receive the Certificate of Attendance, you may pay the fee from the payment below </p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                  
                  <div>
                    
                    <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                      <input type="hidden" name="cmd" value="_s-xclick">
                      <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                      <table>
                        <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                        
                        <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                        <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                        <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                      </select> </td></tr>
                    </table>
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </div>
  </div>
  <!-- 2nd row -->
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="regdata">
        <div class="jumbotron jumbotron-fluid" >
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1 class="display-4">Register for the Tolerance in Sports and Sports Media</h1>
                <hr>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCqDNUnNfbyZCwiXtAFFrXkPgtH2rhW7QCgCEYv62fVnt6WA/viewform">
                  <div class="google-reg aregistration">
                    <img src="images/reg-400x300.png" alt="">
                  </div>
                </a>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead">To register for the Tolerance in Sports and Sports Media, please click here to fill in the form. If you have already registered, please do not register again</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead"> If you would like to receive the Certificate of Attendance, you may pay the fee from the payment below</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                
                <div>
                  
                  <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                    <table>
                      <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                      
                      <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                      <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                      <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                    </select> </td></tr>
                  </table>
                  <input type="hidden" name="currency_code" value="USD">
                  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

  <!-- 3rd row -->
  <div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
      <div class="regdata">
        <div class="jumbotron jumbotron-fluid" >
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h1 class="display-4">Register for the Tolerance in Education</h1>
                <hr>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <a href="https://docs.google.com/forms/d/e/1FAIpQLScIJwKotyECtOdhEp0Gk7lblSdSh7KPjfyEf3RF8LNGz9Dagg/viewform?vc=0&c=0&w=1&flr=0">
                  <div class="google-reg aregistration">
                    <img src="images/reg-400x300.png" alt="">
                  </div>
                </a>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead">To register for the Tolerance in Education, please click here to fill in the form. If you have already registered, please do not register again.</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                <p class="lead"> If you would like to receive the Certificate of Attendance, you may pay the fee from the payment below</p>
              </div>
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6 mt-4">
                
                <div>
                  
                  <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">
                    <table>
                      <tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates  </td></tr><tr><td><select name="os0">
                      
                      <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>
                      <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>
                      <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>
                    </select> </td></tr>
                  </table>
                  <input type="hidden" name="currency_code" value="USD">
                  <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                  <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

</div>
</section>
<section class="home-register">
<div class="container">
<div class="row d-flex justify-content-center align-items-center">
  <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
    
  </div>
</div>
</div>
</section>
<?php include('include/main_footer.php'); ?>