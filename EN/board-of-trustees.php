<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">Board Of Trustees</h1>
			</div>
		</div>
	</div>
</div>
<div class="site-section section-6 advisory-board-section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 text-center">
				<h2>BOARD OF TRUSTEES</h2>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/HE-Sheikh-Eng.-Salem.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>His Excellency Engineer Sheikh Salem Bin Sultan Al Qasimi</h3>
					<p>Chairman,<br/>
					Ras Al Khaima Civil Aviation</p>
					<p>United Arab Emirates</p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/Dr.-King-V.-Cheek.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>Dr. King V. Cheek</h3>
					<p>Chairman of the Board of Trustees<br/>
					International Foundation for Tolerance</p>
					<p>New York, USA</p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/joseph 2.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>Bishop Dr. Joseph K. Grieboski</h3>
					<p>President ,<br/> International Center on Religion and Justice
					Washington DC,</p>
					<p>United States of America</p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/Nicholas-Cardy-1.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>Nicholas Cardy</h3>
					<p>International journalist</p>
					<p>United Kingdom</p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/Dr.-James-Chang.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>Dr. James Chang</h3>
					<p>President <br/> Global Knowledge Exchange Foundation</p>
					<p>United States of America </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/layla.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					
					<h3>Dr. Layla Al Bloushi</h3>
					<p>CEO,<br/>
					AlFikrah Management Consulting</p>
					<p>United Arab Emirates </p>
				</div>
			</div>
			<div class="col-lg-4 mr-auto  mt-5">
				<div class="testimony-1">
					<div class="d-flex align-items-center mb-4">
						<img src="images/Ahdiya-Ahmed.jpg" alt="Image" class="vcard mr-4">
						<div>
							
							<!-- <p>Business Man</p> -->
						</div>
					</div>
					<h3>Ahdiya Ahmed</h3>
					<p>President<br/>
					Bahrain Journalists Association</p>
					<p>Bahrain</p>
				</div>
			</div>

		</div>
	</div>
</div>
<div class="site-section section-1 section-1-about bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 mb-md-4 text-center">
				<h1>THANK YOU!</h1>
				<h3>Lorem ipsum dolor, sit amet consectetur</h3>
				<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Dicta sequi quo vel ipsum laboriosam aliquam laborum officia atque fugiat. Sunt unde expedita labore ea dolorum officia reprehenderit repellendus quos laboriosam.</p>
			</div>
			
			<div class="col-lg-4 mb-md-4 section-title">
				<h2>Lorem </h2>
				<p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia fuga nisi </p>
			</div>
			<div class="col-lg-8">
				<div class="px-lg-3">
					<p class="dropcap">Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nisi, laudantium? Similique mollitia ut, nisi voluptas expedita ex corrupti eaque excepturi nesciunt deserunt harum aliquam, tempore velit error facilis quam voluptates.</p>
					
					<ul>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						<li><a href="javascript:void();">Lorem ipsum dolor</a></li>
						
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</div>
<?php include('include/main_footer.php'); ?>