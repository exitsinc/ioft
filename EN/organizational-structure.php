<?php include('include/main_header.php'); ?>
<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row align-items-center justify-content-center">
			<div class="col-md-8 text-center" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4">Organizational Structure</h1>
			</div>
		</div>
	</div>
</div>
<div class="site-section section-1 section-1-about bg-light">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 mb-md-4 section-title">
				<h2>LOREM</h2>
				
			</div>
			<div class="col-lg-9">
				<div class="px-lg-3">
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Saepe eius illum animi porro voluptate ipsa similique suscipit debitis quasi, maxime consequatur magni dolorum tempore optio quam nam, facere iure quos.</p>
					<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ex fugit alias in, enim aperiam, ad quo nisi voluptate omnis, dolore exercitationem libero atque iste est veniam laudantium iure fuga tenetur.</p>
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque enim perferendis inventore amet soluta quod, modi, ut quaerat placeat reprehenderit provident eveniet! Corporis alias facilis deleniti praesentium labore perspiciatis possimus!</p>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-lg-3 mb-md-4 section-title">
				<h2>LOREM</h2>
				
			</div>
			<div class="col-lg-9">
				<div class="px-lg-3">
					<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis eum necessitatibus iste ipsam, laudantium, maiores eius fugiat nesciunt quas aut quibusdam laborum voluptates ad, quod repudiandae molestiae deserunt natus minima.</p>
					<p>Lorem ipsum, dolor sit amet, consectetur adipisicing elit. Eveniet perspiciatis, consequuntur obcaecati illum dolorem possimus doloribus veritatis saepe neque voluptatibus dolor suscipit provident tenetur eius repellat ducimus repellendus esse culpa.</p>
				</div>
			</div>
			
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card mb-3">
					<div class="row no-gutters">
						<div class="col-lg-3 col-md-4">
							<div class="card-body">
								<h3 class="card-title">Lorem ipsum dolor, sit amet </h3>
							</div>
						</div>
						<div class="col-md-9">
							<div class="card-body">
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								<p class="card-text">Lorem ipsum dolor, sit amet consectetur</p>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
<?php include('include/main_footer.php'); ?>