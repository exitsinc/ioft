<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>About Us</h1>
      </div>
    </div>
    </div>
  </div>
<!-- mission -->
 <div class="row" style="position: relative; z-index: 0;">
                <div class="col-lg-6">
                    <div class="single_feature text-center" style="padding: 50px;">
                        <div class="icon">
                            <i class="fa fa-line-chart" aria-hidden="true"></i>
                        </div>
                        <h3>Mission</h3>
                        <p style="text-align: justify;">
                            Our mission is to contribute to building bridges of understanding and acceptance among different people, groups and cultures through creating dialogue based on mutual respect, learning and research for a safer living environment for all humankind
                        </p>
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="single_feature text-center" style="padding: 50px;">
                        <div class="icon">
                            <i class="fa fa-building-o" aria-hidden="true"></i>
                        </div>
                        <h3>Vision</h3>
                        <p style="text-align: justify;">To be the first choice for learning on cultural conflict resolution  and building sustainable bridges among diversified cultures for the betterment of humankind.

                        </p>
                        
                    </div>
                </div>
            </div>
<!-- board of trustee -->
<div class="site-section section-3">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-lg-6 section-title text-center">
            <h2>Board of Trustees </h2>
          </div>
        </div>

         <div class="section-2">
      <div class="container">
        <div class="row no-gutters align-items-stretch align-items-center">
          <div class="col-lg-3">
            <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                   <span class="flaticon-law"></span>
                </span>
                <h2>Dr. King V. Cheek</h2>
                <p>Chairman of the Board of Trustees 
                  New York College of Health Professions 
                  New York, USA</p>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="service-1 first h-100" style="background-image: url('images/img_1.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                   <span class="flaticon-law"></span>
                </span>
                <h2>Nicholas Cardy </h2>
                <p>International journalist
                   United Kingdom
                 </p>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="service-1 h-100" style="background-image: url('images/img_2.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                  <span class="flaticon-law"></span>
                </span>
                <h2>Dr. Layla Albloushi </h2>
                <p>Chairman 
                  AlFikrah Management Consulting 
                 United Arab Emirates 
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-3">
            <div class="service-1 h-100" style="background-image: url('images/img_3.jpg')">
              <div class="service-1-contents">
                <span class="wrap-icon">
                   <span class="flaticon-law"></span>
                </span>
                <h2>Dr. James Chang </h2>
                <p>President 
                   Global Knowledge Exchange Foundation 
                   United States of America 
                 </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

<!-- advisory board -->

<?php include('include/main_footer.php'); ?>