<?php include('include/main_header.php'); ?>
<!-- <section class="topics-ban">
	<div class="top-ban">
		<img src="images/gray-painted-background_53876-94041.jpg" class="d-block w-100" alt="...">
		<h1 style="margin: 0 auto;">Themes & Topics</h1>
	</div>
</section> -->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1 class="mb-4">محاور وموضوعات المؤتمر</h1>
      </div>
    </div>
  </div>
</div>


<section class="topic-main mtb text-right">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8">
				<h5>ينعقد المؤتمر الاقليمي الاسبوي للتسامح عبر الثقافات تحت شعار ... نحو بناء جسور التفاهم والقبول بين  الثقافات اللخير الانسانية </h5>
        <h5>وسيناقش المؤتمر من خلال البحوث واوراق العمل المقدمة عدة موضوعات منها </h5>
           <ul dir="rtl">
           	<li> مفهوم التسامح </li>
           	<li> عناصر التسامح والفهم والقبول </li>
           	<li> التسامح في الاديان </li>
           	<li> التسامح في فكر زايد </li>
           	<li> التسامح ضمن الاطار المؤسسي مع مختلف الجنسيات</li>
           	<li> التسامح ضمن الاطار المجتمعي والتنوع الثقافي </li>
           	<li> كيفية ترسيخ مفاهيم التسامح مع الناشئة </li>
           	<li> تطبيقات التسامح في الرياضة </li>
           	<li> تطبيقات التسامح في الاعلام </li>
           <li>  افضل الممارسات عالميا </li>
           <li>  موضوعات اخرى</li>
           </ul>
			</div>				
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
				<img src="images/img_4.jpg" class="d-block w-100">
			</div>				
		</div>
	</div>
</section>
<?php include('include/main_footer.php'); ?>