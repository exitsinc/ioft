<?php include('include/main_header.php'); ?>

<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
       <h1 class="mb-4">تبرعات </h1>
      </div>
    </div>
  </div>
</div>



<section class="donate mtb right-text-class">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="jumbotron">
					<h1 class="display-4"> تبرع لدعم نشاطات الهيئة الدولية للتسامح </h1>
					<p class="lead">الهيئة الدولية للتسامح هي مؤسسة غير ربحية وتعتمد في تمويل فعالياتها على تطوع الافراد وتبرعات الافراد والمؤسسات ... يمكنك التبرع لدعم نشاطاتنا باي مبلغ حيث لا يوجد هناك ما يسمي مبلغ صغير او بسيط فكل دولار له اهميته .
					</p>
					<!-- <p>نرجو كتابة الاسم والايميل ثم الانتقال الى الدفع بالبطاقة الائتمانية كما يسعدنا الرد على استفساراتكم على الايميل  <a href="info@iftolerance.org">info@iftolerance.org</a> -->
					<ul dir="rtl">
						<li>10  دولار</li>
						<li>25 دولار</li>
						<li>50 دولار</li>
						<li>100 دولار</li>
						<li>1000 دولار</li>
					</ul>
					<hr class="my-4">
					<p>نرجو كتابة الاسم والايميل ثم الانتقال الى الدفع بالبطاقة الائتمانية كما يسعدنا الرد على استفساراتكم على الايميل </p>
					<a href="info@iftolerance.org">info@iftolerance.org</a>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="donation-form mtb right-text-class">
	<div class="container dnt-form">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="heading">
					<h1>تبرعات
					</h1>
					<hr>
				</div>
				<form>
					<div class="form-row">
						<div class="form-group col-md-12">
							<label for="inputName">الاسم </label>
							<input type="text" class="form-control" placeholder=" ">
						</div>
						
					</div>
					<!-- <div class="form-row">
							<div class="form-group col-md-6">
									<label for="inputOrganization">Organization</label>
									<input type="text" class="form-control" placeholder="Organization">
							</div>
							<div class="form-group col-md-6">
									<label for="inputWorkphone">Work Phone</label>
									<input type="text" class="form-control" placeholder="Work Phone">
							</div>
					</div> -->
					<div class="form-row">
						<div class="form-group col-md-6">
							<label for="inputEmail4">الايميل </label>
							<input type="email" class="form-control" id="inputEmail4" placeholder=" ">
						</div>
						<div class="form-group col-md-6">
							<label for="inputLastname">رقم التلفون</label>
							<input type="text" class="form-control" placeholder=" ">
						</div>
					</div>
					<fieldset class="form-group">
						<div class="row">
							<legend class="col-form-label col-sm-2 pt-0">Payment frequency</legend>
							<div class="col-sm-10">
								<div class="form-check">
									<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
									<label class="form-check-label" for="gridRadios1">
										تبرع مرة واحدة
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
									<label class="form-check-label" for="gridRadios2">
										تبرع شهري
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3">
									<label class="form-check-label" for="gridRadios3">
										تبرع سنوي
									</label>
								</div>
								<!-- <div class="form-check">
										<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios4" value="option4">
										<label class="form-check-label" for="gridRadios4">
												Semi-annually
										</label>
								</div>
								<div class="form-check">
										<input class="form-check-input" type="radio" name="gridRadios" id="gridRadios5" value="option5">
										<labelnually
											</labe class="form-check-label" for="gridRadios5">
												Anl>
									</div>
								</div> -->
							</div>
						</fieldset>
						<div class="form-group">
							<label for="inputAddress">العنوان</label>
							<input type="text" class="form-control" id="inputAddress" placeholder="">
						</div>
						<!-- <div class="form-row">
								<div class="form-group col-md-3">
										<label for="inputCity">City</label>
										<input type="text" class="form-control" id="inputCity">
								</div>
								<div class="form-group col-md-3">
										<label for="inputCountry">Country</label>
										<input type="text" class="form-control" id="inputCountry">
								</div>
								<div class="form-group col-md-3">
										<label for="inputState">State</label>
										<input type="text" class="form-control" id="inputState">
								</div>
								<div class="form-group col-md-3">
										<label for="inputZip">Zip</label>
										<input type="text" class="form-control" id="inputZip">
								</div>
						</div> -->
						<div class="form-group">
							<label for="inputCity">ملاحظات اخرى</label>
							<textarea type="text" class="form-control" id="inputCity"></textarea>
						</div>
						<!-- <div class="form-row form-group">
								<div class="form-group col-md-3">
										<label for="inputEmail4">Security check code :</label>
										<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
								</div>
								<div class="form-group col-md-3">
								<label for="inputEmail4"><?php echo uniqid(); ?></label>
								<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
							</div>
							<div class="form-group col-md-6">
								<label for="inputPassword4">Code</label>
								<input type="Amount" class="form-control" id="inputAmount" placeholder="Enter Security check code">
							</div>
						</div> -->
						<div class="form-group row">
							<div class="col-sm-6 pb-2">
								<button type="Cancel" class="btn btn-primary">Cancel</button>
							</div>
							<div class="col-sm-6">
								<!-- <div id="paypal-button-container"></div> -->
								<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

<table>

<tr><td><input type="hidden" name="on0" value="Certificates شهادات">شهادات</td></tr><tr><td><select name="os0">

                <option value="المؤتمر الاسيوي للتسامح">المؤتمر الاسيوي للتسامح $25.00 USD</option>

                <option value="مؤتمر التسامح في الرياضة">مؤتمر التسامح في الرياضة $30.00 USD</option>

                <option value="مؤتمر التسامح في التعليم">مؤتمر التسامح في التعليم $30.00 USD</option>

                <!-- <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>

                <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>

                <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option> -->

</select> </td></tr>

</table>

<input type="hidden" name="currency_code" value="USD">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

</form>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<?php include('include/main_footer.php'); ?>