<?php include('include/main_header.php'); ?>
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>The Asian Regional Tolerance Across Cultures Conference<br>November 16-17,2020</h1>
      </div>
    </div>
  </div>
</div>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-painted-background_53876-94041.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Conferences</h1>
       
      </div>
    </div>
  </div>
</div> -->
<section class="conferences-main pt-5">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
        <div class="jumbotron">
          <div class="row d-flex justify-content-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-3 col-xl-3 d-flex justify-content-center align-items-center">
              <div class="conference-img">
                <img src="images/King.jpg" alt="">
              </div>
            </div>
          </div>
          <!-- <h1 class="display-4">Hello, world!</h1> -->
          <p class="lead">On behalf of my colleagues, members of the Board of Trustees and Advisory Board of the International Foundation for Tolerance, I greet you here with warm words of love to invite you to join us at our Asian Regional Tolerance Across Cultures Conference. The Conference is for two days November 16-17,2020. Due to the current global health situation, the conference will be held virtually through a conference online platform such as Zoom.</p>
          <p class="lead">We have a selected panel of international speakers and researchers from different part of the world. The conference will feature 2-3 concurrent sessions in Arabic and 1-2 in English. This is to provide everybody with more choices of sessions. We will distribute all the conference papers to all registered delegates.</p>
          <p class="lead">The conference is FREE and no registration fees to be paid. However, for those who may request a certificate of attendance, there will be a small fee for that.</p>
          <p class="lead">Looking forward to meet you virtually at the Conference, we request to take care and stay safe.</p>
          <p class="lead">Dr. King V Cheek <br> Conference Chair</p>
          <!-- <hr class="my-4">
          <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
          <p class="lead">
            <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
          </p> -->
        </div>
      </div>
    </div>
  </div>
</section>
<!-- <section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center" style="border: 2px solid;
    border-radius: 15px;
    margin: 0 auto;
    ">
    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
      <a href="https://docs.google.com/forms/d/e/1FAIpQLSe9lbCiIDTzj5Aorakbj5LIZRUttHtWCI1hLUcPRS5Lux3m7Q/viewform?vc=0&c=0&w=1&flr=0">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
      </a>
    </div>
    <div class="col-md-6">

      <form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

        <input type="hidden" name="cmd" value="_s-xclick">

        <input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

        <table>

          <tr><td style="font-size: 24px;font-weight:600;"><input type="hidden"  name="on0" value="Certificates شهادات">Certificates  </td></tr>
          <tr><td style="margin-bottom:10px;font-size: 20px;"><select name="os0">



            <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>

            <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>

            <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>

          </select> </td></tr>

        </table>

        <input type="hidden" name="currency_code" value="USD" >

        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!" style="margin-top:10px;height: 30px;">

        <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

      </form>
    </div>
  </div>
</div>
</section> -->
<section class="home-register pt-5">
  <div class="container">
    <div class="row d-flex justify-content-center align-items-center">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 col-xl-6">
        <a href="https://docs.google.com/forms/d/e/1FAIpQLSfCqDNUnNfbyZCwiXtAFFrXkPgtH2rhW7QCgCEYv62fVnt6WA/viewform">
        <div class="google-reg">
          <img src="images/reg-400x300.png" alt="">
        </div>
        </a>
      </div>
    </div>
  </div>
</section>
<?php include('include/main_footer.php'); ?>