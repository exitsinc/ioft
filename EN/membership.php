<?php include('include/main_header.php'); ?>
<!-- <div class="site-blocks-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
	<div class="container">
		<div class="row d-flex align-items-center justify-content-center">
			<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
				<h1 class="mb-4 membership-h2">Membership</h1> -->
				<!-- <p class="mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Praesentium facere officiis in quo, corporis quasi.</p>
				<p><a href="#" class="btn btn-primary px-4 py-3">Get Started</a></p> -->
	<!-- 		</div>
		</div>
	</div>
</div> -->
<div class="site-section section-4 home-page-banner" style="background-image: url('images/gray-background-3.jpg');">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center text-white">
        <h1>Membership</h1>
      </div>
    </div>
    </div>
  </div>
<!-- <div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('images/gray-background-3.jpg');" data-aos="fade" data-stellar-background-ratio="0.5" data-aos="fade">
  <div class="container">
    <div class="row d-flex align-items-center justify-content-center">
      <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8 col-xl-8 text-center banner-div" data-aos="fade-up" data-aos-delay="400">
        <h1 class="mb-4">Membership</h1>
      </div>
    </div>
  </div>
</div> -->
<!-- <section class="benifits">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="jumbotron">
					<h2 class="display-4 membership-display-4"><span>Be Active</span> and join the International Foundation for Tolerance today. Please fill in the membership form and start your membership.</h2>
					<h2 class="lead mt-4 membership-p">Membership Benefits</h2>
					<hr class="my-4">
					<p>By being a member of the International Foundation for Tolerance you will</p>
					<ul>
						<li>Receive a certificate of membership</li>
						<li>Receive a membership number to use whenever communicating with us</li>
						<li>Receive our monthly newsletter</li>
						<li>Receive invitations to all our activities</li>
						<li>Receive special discounts on conferences and events</li>
						<li>Receive invitations to participate in different projects being launched</li>
						<li>You can volunteer for different assignments</li>
					</ul>
					<h2 class="lead mt-4 membership-p">Membership Fees</h2>
					<hr class="my-4">
					<p>The annual membership fee is only US$ 25.00 </p>
					<hr class="my-4">
					<div class="crt">
						<img src="images/crt 102.jpg " class="d-block w-100">
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<<!-- section class="denger">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="jumbotron mt-5 text-danger">
					<p>I have read the mission and vision statements of the International Foundation for Tolerance. By applying for membership, I will do what I can to build bridges of understanding and acceptance of others for the betterment of humanity.</p>
				</div>
			</div>
		</div>
	</div>
</section> -->
<div class="site-section section-6">
      <div class="container">
        <div class="row">
          <div class="col-6">
            <img src="images/img_4.jpg" style="border: 1px solid #ddd;
                                                border-radius: 4px;
                                                padding: 5px;
                                                width: 450px;">
          </div>
          <div class="col-6">
            <p style="text-align: justify;">The International Organization for Tolerance provides a set of benefits and learning opportunities for its members. Members are the real strength of the organization because we work through our members and their voluntary contributions. </p>
            <b>Please join us now…</b>
          </div>
        </div>
      </div>
    </div>
    
    <div class="site-section section-6">
      <div class="container">
        <div class="row">
          <div class="col-10">
          <h2 style="padding-bottom: 20px;">Membership Benefits</h2>
          <li>Receive a certificate of membership</li>
          <li>Receive a membership number to use whenever communicating with us</li>
          <li>Receive our monthly newsletter</li>
          <li>Receive invitations to all our activities</li>
          <li>Receive special discounts on conferences and events</li>
          <li>Receive invitations to participate in different projects being launched</li>
          <li>Receive information on many volunteer assignments and opportunities</li>
        <b>Membership Fees</b>
        <p style="font-weight: bold;">The annual membership fee is GBP 20 ( 20 Sterling Pounds) or US$ 25.00 </p>
        </div>
      </div>
      </div>
    </div>
<section class="membership-form">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
				<div class="mem-form">
					<h2>Membership Application Form</h2>
					<hr>
					<form>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputName">Your Name</label>
								<input type="name" class="form-control" id="inputName">
							</div>
							<div class="form-group col-md-6">
								<label for="inputEmail4">Email</label>
								<input type="email" class="form-control" id="inputEmail4">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputDob">Date of Birth</label>
								<input type="dob" class="form-control" id="inputDob">
							</div>
							<div class="form-group col-md-6">
								<label for="inputCity">City</label>
								<input type="city" class="form-control" id="inputCity">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="inputDob">Telephone Number</label>
								<input type="dob" class="form-control" id="inputDob">
							</div>
							<div class="form-group col-md-6">
								<label for="inputCity">Whatsapp number/Skype</label>
								<input type="city" class="form-control" id="inputCity">
							</div>
						</div>
						<div class="form-row">
							<div class="form-group">
								<label>Are you a student ? </label>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
									<label class="form-check-label" for="inlineCheckbox1">Yes</label>
								</div>
								<div class="form-check form-check-inline">
									<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
									<label class="form-check-label" for="inlineCheckbox2">No</label>
								</div>
							</div></div>
							<div class="form-group">
								<label for="inputAddress2">If yes write name of school/college</label>
								<input type="text" class="form-control" id="inputAddress2" placeholder="College or School Name">
							</div>
							<div class="form-row">
								<div class="form-group">
									<label>Are you employed ?  </label>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
										<label class="form-check-label" for="inlineCheckbox1">Yes</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
										<label class="form-check-label" for="inlineCheckbox2">No</label>
									</div>
								</div></div>
								<div class="form-group">
									<label for="inputAddress2">If yes write Name of employer</label>
									<input type="text" class="form-control" id="inputAddress2" placeholder="Employer Name">
								</div>
								<div class="form-row">
									<div class="form-group">
										<label>Are you self-employed/freelancer ? </label>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" id="inlineCheckbox1" value="option1">
											<label class="form-check-label" for="inlineCheckbox1">Yes</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" id="inlineCheckbox2" value="option2">
											<label class="form-check-label" for="inlineCheckbox2">No</label>
										</div>
									</div></div>
									<div class="form-group">
										<label for="inputAddress2">If yes write Company Name and Website</label>
										<input type="text" class="form-control" id="inputAddress2" placeholder="Company or Website Name">
									</div>
									<div class="form-group ">
										<label for="inputCity">Other comments</label>
										<textarea type="text" class="form-control" id="inputCity"></textarea>
									</div>
									<!-- <div class="form-group ">
													<button type="submit" class="btn btn-primary">Sign in</button>
									</div> -->
									<!-- <div class="form-row form-group">
													<div class="form-group col-md-3">
																	<label for="inputEmail4">Security check code :</label>
																	<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
													</div>
													<div class="form-group col-md-3">
											<label for="inputEmail4"><?php echo uniqid(); ?></label>
											<input type="email" class="form-control" id="inputEmail4" placeholder="Email">
										</div>
										<div class="form-group col-md-6">
											<label for="inputPassword4">Code</label>
											<input type="Amount" class="form-control" id="inputAmount" placeholder="Enter Security check code">
										</div>
									</div> -->
									<div class="form-group row">
										<div class="col-sm-6">
											<button type="Cancel" class="btn btn-primary">Cancel</button>
										</div>
										<div class="col-sm-6">
											<!-- <div id="paypal-button-container"></div> -->
											<form target="paypal" action="https://www.paypal.com/cgi-bin/webscr" method="post">

<input type="hidden" name="cmd" value="_s-xclick">

<input type="hidden" name="hosted_button_id" value="QNMALURH5G3LL">

<table>

<tr><td><input type="hidden" name="on0" value="Certificates شهادات">Certificates </td></tr><tr><td><select name="os0">

                <!-- <option value="المؤتمر الاسيوي للتسامح">المؤتمر الاسيوي للتسامح $25.00 USD</option>

                <option value="مؤتمر التسامح في الرياضة">مؤتمر التسامح في الرياضة $30.00 USD</option>

                <option value="مؤتمر التسامح في التعليم">مؤتمر التسامح في التعليم $30.00 USD</option> -->

                <option value="Asian Tolerance">Asian Tolerance $25.00 USD</option>

                <option value="Tolerance in Sports">Tolerance in Sports $30.00 USD</option>

                <option value="Tolerance in Education">Tolerance in Education $30.00 USD</option>

</select> </td></tr>

</table>

<input type="hidden" name="currency_code" value="USD">

<input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_cart_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">

<img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">

</form>
											<!-- <button type="pay" class="btn btn-primary">Pay</button> -->
										</div>
									</form>
									
								</div>
							</div>
						</div>
					</div>
				</section>
				<?php include('include/main_footer.php'); ?>